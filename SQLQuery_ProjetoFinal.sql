create table Papel
(
	IdPapel int identity(1,1) not null,
	DescricaoPapel varbinary(200) not null,
	primary key (IdPapel)
)

create table Usuario
(
	IdUsuario int identity(1,1) not null,
	NomeUsuario varchar(200) not null,
	SenhaUsuario varchar(35) not null,
	primary key(IdUsuario)
)

create table UsuarioPapel
(
	IdPapel int not null,
	IdUsuario int not null,
	primary key(IdPapel, IdUsuario)
)

Alter table UsuarioPapel
Add Constraint IdPapel Foreign key(IdPapel) References Papel(IdPapel)

Alter table UsuarioPapel
Add Constraint IdUsuario Foreign key(IdUsuario) References Usuario(IdUsuario)


create table TipoClassificacao
(
	IdTipoClassificacao int identity(1,1) not null,
	Descricao varchar(100) not null,
	primary key(IdTipoClassificacao)
)

create table Classificacao
(
	IdClassificacao int identity(1,1) not null,
	Data datetime not null,
	IdTipoClassificacao int not null,
	IdTicket int not null,
	primary key(IdClassificacao)
)

Alter table Classificacao
Add Constraint IdTipoClassificacao Foreign key(IdTipoClassificacao) References TipoClassificacao(IdTipoClassificacao)

create table TipoAtendimento
(
	IdTipoAtendimento int identity(1,1) not null,
	Descricao varchar(200) not null,
	primary key(IdTipoAtendimento)
)

create table Ticket
(
	IdTicket int identity(1,1) not null,
	IdTipoAtendimento int not null,
	Descricao text not null,
	primary key(IdTicket)
)

Alter table Ticket
Add Constraint IdTipoAtendimento Foreign key(IdTipoAtendimento) References TipoAtendimento(IdTipoAtendimento)

Alter table Classificacao
Add Constraint IdTicket Foreign key(IdTicket) References Ticket(IdTicket)

create table Situacao
(
	IdSituacao int identity(1,1) not null,
	Descricao varchar(100) not null,
	primary key(IdSituacao)
)


Create table Movimentacao
(
	IdMovimentacao int identity(1,1) not null,
	IdMovimentacaoOrigem int not null,
	IdTicket int not null,
	IdSituacao int not null,
	Descricao text not null,
	DataMovimentacao datetime not null,
	IdUsuarioMovimentacao int not null,
	primary key(IdMovimentacao)
)

Alter table Movimentacao
Add Constraint IdMovimentacaoOrigem Foreign key(IdMovimentacaoOrigem) References Movimentacao(IdMovimentacao)

Alter table Movimentacao
Add Constraint IdTicketMovimentacao Foreign key(IdTicket) References Ticket(IdTicket)

Alter table Movimentacao
Add Constraint IdSituacao Foreign key(IdSituacao) References Situacao(IdSituacao)

